package tester;

import java.util.Arrays;
import java.util.DuplicateFormatFlagsException;
import java.util.InputMismatchException;
import java.util.Scanner;
import dao.MaxMinImpl;

public class MaxMinMain {

	public static void main(String[] args) throws Exception {
		System.out.println("\nFrom how many numbers you want to find maximum and minimum number ?");
		
		try {
			Scanner sc=new Scanner(System.in);
			int n=sc.nextInt();
		
			MaxMinImpl maxminimpl=new MaxMinImpl(); 		    			//Creating object of class MaxMin to invoke methods
	
			int array[]=new int[n];					   			//Creating new array to store elements dynamically 
										  			 //by user's choice
			System.out.println("\nEnter "+n+" elements : ");
			
			for (int i=0; i<n; i++) 
			 {		
				 array[i]=sc.nextInt();
			 }
				

			System.out.println("\nArray Elements are:"+Arrays.toString(array));		//Display unsorted array
			 
	
			int low = 0;
			int high = array.length - 1;
			
			maxminimpl.sortArray(array, low, high);
		    
			
			if (array == null)
				   System.out.println("array is not initialized");
			else if (array.length < 1)
				   System.out.println("array is empty");					//Checking array is empty or not
			
			
			
			int i=0;
			if(array[i] == array[i+1]){
					System.out.println("No Duplicate elements allowed ");			//Condition applied to avoid duplicate numbers
				}	
			 
			
			System.out.println("\nSorted Array:"+Arrays.toString(array));				//Display sorted array
			
			System.out.println("Minimum element from array:"+array[0]);
			System.out.println("Maximum element from array:"+array[n-1]);
				
		
			}
		
		catch (InputMismatchException e) {								//Exception handled as user 
													  	//gives input other than integer
																										
				System.out.println("\nPlease enter only integers..!!!");
			} 
		}
}
