package dao;
import pojo.MaxMin;


	public interface MaxMinDao {
		
		MaxMin sortArray(int[] arr, int low, int high) throws Exception;	//Method to sort the array
		}
