package dao;
import pojo.MaxMin;

public class MaxMinImpl implements MaxMinDao{
	
	

	@Override
	public MaxMin sortArray(int[] array, int low, int high) throws Exception {
		// TODO Auto-generated method stub
		if (array == null || array.length == 0)
			return null;
 
		if (low >= high)
			return null;

		int middle = low + (high - low) / 2;						// pick the pivot

		int pivot = array[middle];

		int i = low, j = high;										// Condition checking as left<pivot and right>pivot
		while (i <= j) {
			while (array[i] < pivot) {
				i++;
			}
 
			while (array[j] > pivot) {
				j--;
			}
 
			if (i <= j) {											//Swapping the values of i and j
				int temp = array[i];
				array[i] = array[j];
				array[j] = temp;
				i++;
				j--;
			}
		}
 
		if (low < j)												// recursively sort two sub parts
			sortArray(array, low, j);
 
		if (high > i)
			sortArray(array, i, high);
		return null;
	}

	
	

}
