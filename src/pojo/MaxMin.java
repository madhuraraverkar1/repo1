package pojo;
import java.util.Arrays;

public class MaxMin {

	public class MaxMinFromArray {

		private int arr[];						//Declare an array

		public int[] getArr() {					
			return arr;
		}

		public void setArr(int[] arr) {			//Initializing the array
			this.arr = arr;
		}

		@Override
			public String toString() {
			return "MaxMinFromArray [arr=" + Arrays.toString(arr) + "]";
		}
		
		
		
	}
}
