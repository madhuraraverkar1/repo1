package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/TriangleServlet")
public class TriangleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TriangleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
		 int side1= Integer.valueOf(request.getParameter("side1"));    
		 int side2= Integer.valueOf(request.getParameter("side2"));
		 int side3= Integer.valueOf(request.getParameter("side3"));
	     
	         
	        System.out.println("Side1: " + side1);                  //To print the result on console
	        System.out.println("Side2: " + side2);
	        System.out.println("Side3: " + side3);
	    
	        PrintWriter writer = response.getWriter();
	         
	        String htmlResponse= "<html>";
	        htmlResponse += "<h2 align=\"center\">Side1 : " + side1 + "<br/>";      
	        htmlResponse += "<h2 align=\"center\">Side2 : " + side2 + "<br/>";      
	        htmlResponse+= "<h2 align=\"center\">Side3 : " + side3 + "<br/>";      

	        htmlResponse += "</html>";
	        writer.println(htmlResponse);
	       
	        
	        String htmlResponse1 = "<html>";
	         
	        if(side1==side2 && side2==side3)
	        {
	        	htmlResponse1 += "<h2 align=\"center\"  style=\"color:#FF0000\">Triangle Type : Equilateral Triangle  <br/><br/>";
	        	htmlResponse1+= "<img src=//cramster-image.s3.amazonaws.com/definitions/DC-2414V2.png width=\"400\" height=\"233\">";
	        }
	        	
	        else if ((side1==side2 && side2!=side3 ) || (side1!=side2 && side3==side1) || (side3==side2 && side3!=side1))
	        {
	        	htmlResponse1 += "<h2 align=\"center\"  style=\"color:#FF0000\">Triangle Type : Isosceles Triangle  <br/><br/>";
	        	htmlResponse1+= "<img src=//cramster-image.s3.amazonaws.com/definitions/DC-2414V1.png width=\"400\" height=\"233\">";
	        }
	        
	         else if(side1<=0 || side2<=0 || side3<=0)
	       	{	
	        	 htmlResponse1 += "<h2 align=\"center\" style=\"color:#FF0000\">Number should be greater than 0";
	       	}
	        
	         else 
	        {
	        	 htmlResponse1 += "<h2 align=\"center\" style=\"color:#FF0000\">Triangle Type : Scalene Triangle<br/><br/>";
	        	 htmlResponse1+= "<img src=http://www.supercoloring.com/sites/default/files/styles/coloring_full/public/cif/2009/01/triangle-15-coloring-page.gif  width=\"400\" height=\"233\">";
	         }
	        
	       	htmlResponse1 += "</html>";
	        writer.println(htmlResponse1);
		
		}catch(Exception e){
			System.out.println("Please enter valid number");
		}
		

		}
		
	}


