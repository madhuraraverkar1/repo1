import java.util.Scanner;

class EquilateralTriangle {
  public static void main(String [] args) {
	  Scanner sc=new Scanner(System.in);
    int size;
    int side1,side2,side3;
    System.out.println("\nPlease enter three sides of triangle:");
    side1=sc.nextInt();
    side2=sc.nextInt();
    side3=sc.nextInt();
    System.out.println("Side1= "+side1);
    System.out.println("Side2= "+side2);
    System.out.println("Side3= "+side3);
    
    
    
   if((side1==side2 && side2!=side3 ) || (side1!=side2 && side3==side1) || (side3==side2 && side3!=side1))
    {
	     
    	int rows=0;
    	if(side1==side2)
    		rows=side1;
    	else
    		rows=side3;
    	int i;
	    int r,c,blnk=0;

	    for(r=1;r<=side1;r++)
	    {

	                    for(blnk=side1-1; blnk>=r;blnk--)
	                    {
	                    System.out.print(" ");
	                    }
	                    for(c=1;c<=2*r-1;c++)
	                    {
	                    if(r==side1 || c==1 || c== (2*r-1))   
	                    	System.out.print("*");
	                    else                                                       
	                    	System.out.print(" ");   
	                    
	                    }
	    System.out.println();
	    }
    	 
    	System.out.println("\nOnly two sides are equal.So,this is Isosceles Triangle..!!");
    }
   
   
    else if((side1==side2 && side2==side3))
    {    
	    int i, j;
	    for(i=1;i<=side1;i++) {
	      for(j=1;j<=side1;j++) {
	        if((i+j)>side1) {
	          System.out.print("*");
	          System.out.print(" ");
	        } else {
	          System.out.print(" ");
	        }
	        	
	      }
	      System.out.println();
	    }System.out.println("\nAll the sides of triangle are equal.This is Equilateral Triangle..!!");
	   
    }
    else{
    	
	    System.out.println("\nAll the sides of triangle are different.So, this is Scalene Triangle..!!");
	  }
    
   
    
  }
}