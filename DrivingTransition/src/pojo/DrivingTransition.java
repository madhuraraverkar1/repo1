package pojo;

public class DrivingTransition{

	private int time;
	private int vehicle_speed;
	private int engine_speed;
	private int odometer_reading;
	
	public DrivingTransition() {
		super();
	}
	
	public DrivingTransition(int time, int vehicle_speed, int engine_speed, int odometer_reading) {
		super();
		this.time = time;
		this.vehicle_speed = vehicle_speed;
		this.engine_speed = engine_speed;
		this.odometer_reading = odometer_reading;
	}
	
	
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getVehicle_speed() {
		return vehicle_speed;
	}
	public void setVehicle_speed(int vehicle_speed) {
		this.vehicle_speed = vehicle_speed;
	}
	
	public int getEngine_speed() {
		return engine_speed;
	}
	public void setEngine_speed(int engine_speed) {
		this.engine_speed = engine_speed;
	}
	public int getOdometer_reading() {
		return odometer_reading;
	}
	public void setOdometer_reading(int odometer_reading) {
		this.odometer_reading = odometer_reading;
	}
	@Override
	public String toString() {
		return "DrivingTransition [time=" + time + ", vehicle_speed=" + vehicle_speed + ", engine_speed=" + engine_speed
				+ ", odometer_reading=" + odometer_reading + "]";
	}
	
		
	
}
