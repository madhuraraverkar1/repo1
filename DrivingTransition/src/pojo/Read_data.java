package pojo;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import evaluation.DistanceEvaluation;

public class Read_data 
{
	//Delimiters used in the CSV file
    private static final String COMMA_DELIMITER = ",";
    
    public static void main(String args[]) throws FileNotFoundException 
    {
    	DistanceEvaluation d= new DistanceEvaluation();
        BufferedReader br = null;
        BufferedWriter bw = null;
        try
        {
            //Reading the csv file
            br = new BufferedReader(new FileReader("/home/mraverkar/Neon/DrivingTransition/read_data.csv"));
         
            List<DrivingTransition> vehicle = new ArrayList<DrivingTransition>();
            
            String line = "";
            
            br.readLine();
            
            while ((line = br.readLine()) != null) 
            {
                String[] vehicleDetails = line.split(COMMA_DELIMITER);
                
                if(vehicleDetails.length > 0 )
                {
                    //Save the Vehicle details in vehicleRecord object
                	DrivingTransition vehicleRecord = new DrivingTransition(Integer.parseInt(vehicleDetails[0]),Integer.parseInt(vehicleDetails[1]), Integer.parseInt(vehicleDetails[2]),Integer.parseInt(vehicleDetails[3]));
                	vehicle.add(vehicleRecord);
                }
            }
            
            System.out.println("Time\t\tVehicle Speed\t Engine Speed\tOdometer Reading\n"); 
            
            for(DrivingTransition e : vehicle)
            {
                System.out.println(e.getTime()+"\t\t"+e.getVehicle_speed()+"\t\t"
                		+e.getEngine_speed()+"\t\t"+e.getOdometer_reading());
            }
            
             
            d.writeCsvFile("/home/mraverkar/Neon/DrivingTransition/write_data.csv");
            
        }
        catch(Exception ee)
        {
            ee.printStackTrace();
        }
        finally
        {
            try
            {
                br.close();
            }
            catch(IOException ie)
            {
                System.out.println("Error occured while closing the BufferedReader");
                ie.printStackTrace();
            }
        }
    }
}