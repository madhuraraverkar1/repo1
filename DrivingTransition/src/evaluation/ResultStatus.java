package evaluation;

public class ResultStatus {

	int session;
	int start_time;
	int end_time;
	int distance;
	String status;
	
	public ResultStatus() {
		super();
	}
	
	public ResultStatus(int session,int start_time, String status) {
		super();
		this.session = session;
		this.start_time=start_time;
		this.status = status;
	}
	
	
	

	public ResultStatus(int session, int start_time, int end_time, int distance, String status) {
		super();
		this.session = session;
		this.start_time = start_time;
		this.end_time = end_time;
		this.distance = distance;
		this.status = status;
	}

	public int getSession() {
		return session;
	}
	public void setSession(int session) {
		this.session = session;
	}
	public int getStart_time() {
		return start_time;
	}
	public void setStart_time(int start_time) {
		this.start_time = start_time;
	}
	public int getEnd_time() {
		return end_time;
	}
	public void setEnd_time(int end_time) {
		this.end_time = end_time;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

	@Override
	public String toString() {
		return "session=" + session + ", start_time=" + start_time + ", status=" + status ;
	}
}
