package evaluation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pojo.DrivingTransition;
import pojo.Read_data;

public class DistanceEvaluation {
	
		FileWriter fw = null;
		BufferedWriter bw= null;
		private static final String  FILE_HEADER="session,start_time,status";
		static int distance;
		String status;
		static String NEW_LINE_SEPARATOR="\n";
	    //private static final String FILE_HEADER = "session,start_time,end_time,distance,status";
		private static final CharSequence COMMA_DELIMITER = ",";

	
	   File file = new File("/home/mraverkar/Neon/DrivingTransition/write_data.csv");
	   {

    	  if (!file.exists()) 
    	  {
    	     try {
				file.createNewFile();
	              System.out.println("File "
	              		+ " Successfully");
	              
	      			fw = new FileWriter(file,true);
	      		
    	     }
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	     
    	  }
	   }
	   
	   
	   public static void writeCsvFile(String fileName) {
			DrivingTransition dt=new DrivingTransition();
			int speed1=30;
			int time1=5;
			int speed2=10;
			int time2=10;
					
			try {
				BufferedReader br = new BufferedReader(new FileReader("/home/mraverkar/Neon/DrivingTransition/read_data.csv"));
			
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
				
			ResultStatus rs1=new ResultStatus(0,12333, "transition");
			ResultStatus rs2=new ResultStatus(1,0, "transition");
			ResultStatus rs3=new ResultStatus(2, dt.getTime(), "transition");
			
			
			
			ArrayList<ResultStatus> vehicle = new ArrayList();
			vehicle.add(rs1);
			vehicle.add(rs2);
			vehicle.add(rs3);
			
			FileWriter fileWriter = null;
					
			try {
				fileWriter = new FileWriter(fileName);

				
				fileWriter.append(FILE_HEADER.toString());
				
				fileWriter.append(NEW_LINE_SEPARATOR);
				
					fileWriter.write(String.valueOf(vehicle.get(0)));
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(NEW_LINE_SEPARATOR);

					fileWriter.write(String.valueOf(vehicle.get(1)));
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(NEW_LINE_SEPARATOR);

					fileWriter.write(String.valueOf(vehicle.get(2)));
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(NEW_LINE_SEPARATOR);
					

				
				
				System.out.println("\nCSV file was written successfully !!!");
				
			} catch (Exception e) {
				System.out.println("Error in CsvFileWriter !!!");
				e.printStackTrace();
				
			} finally {
				
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e) {
					System.out.println("Error while flushing/closing fileWriter !!!");
	                e.printStackTrace();
				}
				
			}
		}
    	                       
    	     	   
}
